[Appearance]
ColorScheme=ayu

[Cursor Options]
CursorShape=1

[General]
InvertSelectionColors=true
Name=Profile 1
Parent=FALLBACK/
SemanticInputClick=true
SemanticUpDown=true
TerminalCenter=true
TerminalMargin=0

[Interaction Options]
AllowEscapedLinks=true
TextEditorCmd=6
TextEditorCmdCustom=coduim --wait PATH:LINE:COLUMN
UnderlineFilesEnabled=true

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
UrlHintsModifiers=67108864
VerticalLine=false
