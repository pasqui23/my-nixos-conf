{ pkgs, ... }:
{
  home.packages = with pkgs; [
    kdePackages.kate
    omnisharp-roslyn
    rPackages.languageserver
    bash-language-server
    clang-tools
    serve-d
    vscode-langservers-extracted
    typescript-language-server
    dockerfile-language-server-nodejs
    fortls
    glslls
    gopls
    haskell-language-server
    kotlin-language-server
    texlab
    lua-language-server
    nil
    nimlsp
    ocamlPackages.ocaml-lsp
    openscad-lsp
    perlPackages.PerlLanguageServer
    phpactor
    python312Packages.pylsp-rope
    kdePackages.qtdeclarative.dev
    solargraph
    rust-analyzer
    metals
    terraform-ls
    vala-language-server
    vue-language-server
    lemminx
    yaml-language-server
    zls
    marksman
    delve
    python312Packages.debugpy
    asciidoctor
    netcat
    godot_4
    ast-grep

    #missing lsp in nixpkgs
    #graphql-lsp
    #esbonio
    #gdshader-lsp
    #nginx-language-server
    #prolog lsp
    #cobol-language-support
    #haxe-language-server
    #https://github.com/dev-cycles/contextive
    #coffeesense-language-server
    #https://github.com/PowerShell/PowerShellEditorServices
    #sts4
    #julia

    #missings dap in nixpkgs
    #ansibug
    #earlybird

    #kate miss lang def
    #astro-ls
    #glasgow wgsl
    #svelte-language-server
    #uiua
    #jq-lsp
    #jinja-lsp
  ];

  #  taken from https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
  xdg.configFile."kate/lspclient/settings.json".text = builtins.toJSON {
    servers = {
      python.settings.pyls.plugins.pylint.enable = true;
      # 			sonarlint = {
      # 				command = "${pkgs.sonarlint-ls}/bin/sonarlint-ls";
      # 				documentLanguageId=false;
      # 				highlightingModeRegex = ".*";
      # 			};
      matlab = {
        command = "${pkgs.matlab-language-server}/bin/matlab-language-server";
        highlightingModeRegex = "^Matlab$";
        settings.MATLAB = {
          # 					installPath = "";
          matlabConnectionTiming = "onStart";
          telemetry = true;
        };
      };
      # 			ast-grep = {
      # 				command =["ast-gtep""lsp"];
      # 				rootIndicationFilePatterns = ["sgconfig.yml" "sgconfig.yaml"];
      # 				highlightingModeRegex = "(Bash|C|ANSI C89|Objective-C|C\\+\\+|ISO C\\+\\+|C#|CSS|Dart|Elixir|Go|HTML|Java|JavaScript|JSON|Kotlin|Lua|PHP|Rust|TypeScript).*";
      # 			};
      # 			lsp-ai = {
      # 				command = "${pkgs.lsp-ai}/bin/lsp-ai";
      # 				highlightingModeRegex = ".*";
      # 				initializationOptions = {
      # 					memory.file_store={};
      # 					models.local ={
      # 						type = "ollama";
      # 						model = "codestral";
      # 						max_requests_per_second = 1000000;
      # 						chat_endpoint = "http://localhost:${toString os.config.services.ollama.port}/api/chat";
      # 						generate_endpoint = "http://localhost:${toString os.config.services.ollama.port}/api/generate";
      # 					};
      # 				};
      # 			};
      nix.settings.nil = {
        formatting.command = [ "${pkgs.nixfmt-rfc-style}/bin/nixfmt" ];
        nix.flake = {
          autoEvalInputs = true;
          autoArchive = true;
        };
      };
      # 			ltex-ls={
      # 				command = "${pkgs.ltex-ls}/bin/ltext-ls";
      # 				highlightingModeRegex = ".*";
      # 				settings={
      # 					languageTool.motherTongue="it";
      # 					smartFormat.onType=true;
      # 				};
      # 			};
      # 			emmet = {
      # 				cmmand = ["${pkgs.emmet-language-server}/bin/emmet-language-server""--stdio"];
      # 				highlightingModeRegex = "(.*React.*|CSS|.*HTML.*|LESS|SASS|SCSS|Pug|Vue)";
      #
      # 			};
      javascrpt.settings = {
        autoClosingTags = true;
        suggest.autoImports = true;
        suggest.completeFunctionCalls = true;
        updateImportsOnFileMove.enabled = "always";
        preferences.preferTypeOnlyAutoImports = true;
      };
      scala.settings.enableSemanticHighlighting = true;
      # 			spectral={
      # 				command = ["${pkgs.spectral-language-server}/bin/spectral-language-server""--stdio"];
      # 				highlightingModeRegex = "^JSON|YAML$";
      # 			};
      # 			unocss = {
      # 				command =["${pkgs.nodejs}/bin/npx" "-y" "unocss-language-server" "--stdio"];
      # 				highlightingModeRegex = "^(HTML|.*React.*|Vue)";
      # 				rootIndicationFilePatterns = ["unocss.config.js""unocss.config.ts""uno.config.js""uno.conifg.ts"];
      # 			};
      # 			tailwindcss = {
      # 				command =["${pkgs.nodejs}/bin/npx" "-y" "@tailwindcss/language-server"];
      # 				highlightingModeRegex = "^(ASP|Clojure|.*HTML.*|Elixir|.*React.*|Vue|CSS|SASS|SCSS|LESSCSS)";
      # 				rootIndicationFilePatterns = ["tailwind.config.js""tailwind.config.ts"];
      # 			};
      # 			stylelint = {
      # 				command =["${pkgs.nodejs}/bin/npx" "-y" "stylelint-lsp" "--stdio"];
      # 				highlightingModeRegex = "^(.*React.*|Vue|CSS|SASS|SCSS|LESSCSS)";
      # 			};
      toml = {
        command = [
          "${pkgs.taplo-lsp}/bin/taplo"
          "lsp"
          "stdio"
        ];
        highlightingModeRegex = "^TOML$";
      };

      elm.command = "${pkgs.elmPackages.elm-language-server}/lib/node_modules/.bin/elm-language-server";
      clojure = {
        command = "${pkgs.clojure-lsp}/bin/clojure-lsp";
        rootIndicationFilePatterns = [ "project.clj" ];
        highlightingModeRegex = "^Clojure$";
      };
      asm = {
        command = "${pkgs.asm-lsp}/bin/asm-lsp";
        highlightingModeRegex = "^Assembler GNU$";
      };
      java = {
        #TODO: use the NetBeans lsp
        command = "${pkgs.jdt-language-server}/bin/jdtls";
        rootIndicationFilePatterns = [
          "build.gradle"
          "pom.xml"
          "settings.gradle"
          "settings.gradle.ks"
        ];
        highlightingModeRegex = "^Java$";
        settings.gradle.home = pkgs.runCommand "GRADLE_HOME" { inherit (pkgs) gradle; } ''
          mkdir -p $out
          ln -s $gradle/bin $out
          ln -s $gradle/lib/gradle/* $out
        '';
      };
      angular = {
        command = [
          "${pkgs.vscode-extensions.angular.ng-template}/share/vscode/extensions/Angular.ng-template/server/bin/ngserver"
          "--stdio"
          "--tstsProbeLocations"
          "%{Project:NativePath}"
          "--ngProbeLocations"
          "%{Project:NativePath}"
        ];
        root = ".";
        highlightingModeRegex = "^(HTML|TypeScript).*$";
        rootIndicationFilePatterns = [ "angular.json" ];
      };
      #TODO: https://github.com/suoto/hdl_checker
      # Instead or in addition?
      verilog = {
        command = "${pkgs.svls}/bin/svls";
        highlightingModeRegex = "^.*Verilog$";
      };
      vhdl = {
        command = "${pkgs.vhdl-ls}/bin/vhdl_ls";
        highlightingModeRegex = "^VHDL$";
      };
      #       ansible = {
      #         command =["${pkgs.ansible-language-server}/bin/ansible-language-server" "--stdio"];
      # 				rootIndicationFilePatterns = ["ansible.cfg" ".ansible-lint"];
      # 				settings.ansible = {
      # 					python.interpreterPath = "${pkgs.python3}/bin/python";
      # 					validation = {
      # 						enable = true;
      # 						lint = {
      # 							enable = true;
      # 							path = "${pkgs.ansible-lint}/bin/ansible-lint";
      # 						};
      # 					};
      # 				};
      # 				highlightingModeRegex = "YAML$";
      #       };
      #       docker-compose = {
      #         command =["${pkgs.docker-compose-language-service}/bin/docker-compose-language-service" "--stdio"];
      # 				rootIndicationFilePatterns = ["docker-compose.yaml" "docker-compose.yml" "compose.yaml""compose.yml"];
      # 				highlightingModeRegex = "^YAML$";
      #       };
      zk = {
        command = [
          "${pkgs.zk}/bin/zk"
          "lsp"
        ];
        highlightingModeRegex = "^Markdown$";
      };
    };
  };
  # https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation
  xdg.configFile."kate/debugger/dap.json".text = builtins.toJSON {
    dap = {
      netcoredbg = {
        run = {
          command = [
            "${pkgs.netcoredbg}/bin/netcoredbg"
            "--interpreter=vscode"
          ];
          redirectStdout = true;
          redirectStderr = true;
        };
        configurations.launch.request = {
          command = "launch";
          program = ''''${file}'';
          args = ''''${args|list}'';
        };
      };
      lldb-dap = {
        run = {
          command = "${pkgs.lldb}/bin/lldb-vscode";
          redirectStdout = true;
          redirectStderr = true;
        };
        configurations = {
          launch.request = {
            command = "launch";
            program = ''''${file}'';
            args = ''''${args|list}'';
            cwd = ''''${workdir|dir}'';
            env = ''''${env|list}'';
            stopOnEntry = true;
            # TODO: true https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
            # echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
            runInTerminal = false;
          };
          "attach to pid".request = {
            command = "attach";
            program = ''''${file}'';
            pid = ''''${pid|int}'';
          };
          "attach to remote gdb".request = {
            command = "attach";
            program = ''''${file}'';
            attachCommand = [ ''gdb-remote ''${adttess}'' ];
          };
          "attach to name".request = {
            command = "attach";
            program = ''''${file}'';
            waitFor = true;
          };
          "Load coredump".request = {
            command = "attach";
            program = ''''${file}'';
            coreFile = ''''${corefile|path}'';
          };

        };
      };
      nodejs = {
        run = {
          port = 0;
          command = [
            "${pkgs.vscode-js-debug}/bin/js-debug"
            ''''${port}''
          ];
        };
        configurations = {
          "Node (Launch)".request = {
            type = "node2";
            command = "launch";
            program = ''''${file}'';
            args = ''''${args|list}'';
            cwd = ''''${workdir|dir}'';
            env = ''''${env|list}'';
            stopOnEntry = true;
            sourceMaps = true;
            protocol = "inspector";
            console = "integratedTerminal";
            enableDWARF = true;
          };
          "Node (Attach)".request = {
            type = "chrome";
            command = "attach";
            processId = ''''${pid|int}'';
            stopOnEntry = true;
            sourceMaps = true;
            protocol = "inspector";
            console = "integratedTerminal";
          };
          "Chrome (Launch)".request = {
            type = "chrome";
            command = "launch";
            # 						runtimeExecutable = "${pkgs.firefox-devedition}";
            runtimeExecutable = "brave";
            cwd = ''''${workdir|dir}'';
            env = ''''${env|list}'';
            stopOnEntry = true;
            sourceMaps = true;
            protocol = "inspector";
            console = "integratedTerminal";
          };
          "Chrome (Attach)".request = {
            type = "node2";
            command = "attach";
            port = ''''${port}'';
            processId = ''''${pid|int}'';
            stopOnEntry = true;
            sourceMaps = true;
            protocol = "inspector";
            console = "integratedTerminal";
          };
          "Deno (Launch)".request = {
            type = "pwa-node";
            command = "launch";
            program = ''''${file}'';
            args = ''''${args|list}'';
            cwd = ''''${workdir|dir}'';
            env = ''''${env|list}'';
            stopOnEntry = true;
            sourceMaps = true;
            protocol = "inspector";
            console = "integratedTerminal";
            enableDWARF = true;
            runtimeExecutable = "deno";
            runtimeArgs = [
              "run"
              ''--inspect-wait=localhost:''${port}''
              "-A"
            ];
          };
        };
      };
      php = {
        command = [
          "${pkgs.nodejs}/node"
          "${pkgs.vscode-extensions.xdebug.php-debug}/share/vscode/extensions/xdebug.php-debug/out"
        ];
        configurations.launch.request = {
          command = "launch";
          port = 9000;
        };
      };
      haskell = {
        run.command = [
          "${pkgs.haskellPackages.haskell-debug-adapter}"
          "--hackage-version=${pkgs.haskellPackages.haskell-debug-adapter.version}"
        ];
        configurations.launch.request = {
          command = "launch";
          startup = ''''${file}'';
          args = ''''${args|list}'';
          workspace = ''''${workdir|dir}'';
          logFile = ''''${logFile|path}'';
          logLevel = "WARNING";
          ghciEnv = { };
          env = ''''${env|list}'';
          stopOnEntry = true;
          ghciCmd = "stack ghci --test --no-load --no-build --main-is TARGET --ghci-options -fprint-evld-with-show";
        };
      };
      # 			probe-rs-debug = {
      # 				run.command = ["${pkgs.probe-rs}/bin/probe-rs" "dap-server" "--port" ''''${port}''];
      # 			};
      #
      roku-debug = {
        run.command = [
          "${pkgs.nodejs}/bin/npx"
          "-y"
          "roku-debug"
          "--dap"
        ];
        configuration.launch.request = {
          command = "launch";
          program = ''''${file}'';
          args = ''''${args|list}'';
        };
      };
      godot = {
        run.command = "nc 127.0.0.1 6006";
        configuration.launch.request = {
          command = "launch";
          #TODO: the rest of https://github.com/godotengine/godot-vscode-plugin?tab=readme-ov-file#configurations
          profiling = true;
          single_threaded_scene = true;
          debug_collisions = true;
          debug_paths = true;
          debug_navigation = true;
          debug_avoidance = true;
          debug_stringnames = true;
        };
      };
    };
  };
  qt.kde.settings = {
    katerc = {
      BuildConfig = {
        UseDiagnosticsOutput = true;
        AutoSwitchToOutput = true;
      };
      ColorPicker = {
        HexLengths = "3,6,8,9,12";
        NamedColors = true;
        PreviewAfterColor = true;
      };
      General = {
        "Allow Tab Scrolling" = true;
        "Auto Hide Tabs" = true;
        "Close After Last" = false;
        "Close documents with window" = true;
        "Config Revision" = 10;
        "Days Meta Infos" = 99;
        "Diagnostics Limit" = 12000000;
        "Diff Show Style" = 0;
        "Elide Tab Text" = false;
        "Expand Tabs" = false;
        "Icon size for left and right sidebar buttons" = 20;
        "Modified Notification" = true;
        "Mouse back button action" = 1;
        "Mouse forward button action" = 1;
        "Open New Tab To The Right Of Current" = true;
        "Output History Limit" = 100;
        "Quickopen Filter Mode" = 0;
        "Quickopen List Mode" = true;
        "Recent File List Entry Count" = 99;
        "Restore Window Configuration" = true;
        "SDI Mode" = false;
        "Save Meta Infos" = true;
        "Session Manager Sort Column" = 0;
        "Session Manager Sort Order" = 0;
        "Show Full Path in Title" = true;
        "Show Menu Bar" = true;
        "Show Status Bar" = true;
        "Show Symbol In Navigation Bar" = true;
        "Show Tab Bar" = true;
        "Show Tabs Close Button" = true;
        "Show Url Nav Bar" = true;
        "Show output view for message type" = 1;
        "Show text for left and right sidebar" = false;
        "Show welcome view for new window" = true;
        "Startup Session" = "last";
        "Stash new unsaved files" = true;
        "Stash unsaved file changes" = true;
        "Sync section size with tab positions" = false;
        "Tab Double Click New Document" = true;
        "Tab Middle Click Close Document" = true;
        "Tabbar Tab Limit" = 0;
      };
      "KTextEditor Document" = {
        "Allow End of Line Detection" = true;
        "Auto Detect Indent" = true;
        "Auto Reload If State Is In Version Control" = true;
        "Auto Save" = true;
        "Auto Save Interval" = 30;
        "Auto Save On Focus Out" = true;
        "BOM" = false;
        "Backup Local" = true;
        "Backup Remote" = true;
        "Camel Cursor" = true;
        "Encoding" = "UTF-8";
        "End of Line" = 0;
        "Indent On Backspace" = true;
        "Indent On Tab" = true;
        "Indent On Text Paste" = true;
        "Indentation Mode" = "normal";
        "Indentation Width" = 2;
        "Keep Extra Spaces" = true;
        "Line Length Limit" = 10000;
        "Newline at End of File" = true;
        "On-The-Fly Spellcheck" = true;
        "Overwrite Mode" = false;
        "PageUp/PageDown Moves Cursor" = false;
        "Remove Spaces" = 1;
        "ReplaceTabsDyn" = false;
        "Show Spaces" = 1;
        "Show Tabs" = true;
        "Smart Home" = true;
        "Swap File Mode" = 1;
        "Swap Sync Interval" = 15;
        "Tab Handling" = 1;
        "Tab Width" = 2;
        "Trailing Marker Size" = 2;
        "Word Wrap" = false;
        "Word Wrap Column" = 199;
      };
      "KTextEditor View" = {
        "Allow Mark Menu" = true;
        "Auto Brackets" = true;
        "Auto Center Lines" = 0;
        "Auto Completion" = true;
        "Auto Completion Preselect First Entry" = true;
        "Backspace Remove Composed Characters" = true;
        "Bookmark Menu Sorting" = 0;
        "Bracket Match Preview" = true;
        "Default Mark Type" = 1;
        "Dynamic Word Wrap" = true;
        "Dynamic Word Wrap Align Indent" = 10;
        "Dynamic Word Wrap At Static Marker" = false;
        "Dynamic Word Wrap Indicators" = 1;
        "Dynamic Wrap not at word boundaries" = false;
        "Enable Accessibility" = true;
        "Enable Tab completion" = true;
        "Enter To Insert Completion" = true;
        "Fold First Line" = true;
        "Folding Bar" = true;
        "Folding Preview" = true;
        "Icon Bar" = true;
        "Input Mode" = 0;
        "Keyword Completion" = true;
        "Line Modification" = true;
        "Line Numbers" = true;
        "Max Clipboard History Entries" = 20;
        "Maximum Search History Size" = 100;
        "Mouse Paste At Cursor Position" = false;
        "Multiple Cursor Modifier" = 201326592;
        "Persistent Selection" = false;
        "Scroll Bar Marks" = true;
        "Scroll Bar Mini Map" = true;
        "Scroll Bar Mini Map All" = true;
        "Scroll Bar Mini Map Width" = 60;
        "Scroll Bar MiniMap" = true;
        "Scroll Bar Preview" = true;
        "Scroll Past End" = true;
        "Search/Replace Flags" = 140;
        "Shoe Line Ending Type in Statusbar" = true;
        "Show Documentation With Completion" = true;
        "Show File Encoding" = true;
        "Show Focus Frame Around Editor" = true;
        "Show Folding Icons On Hover Only" = true;
        "Show Line Count" = true;
        "Show Scrollbars" = 0;
        "Show Statusbar Dictionary" = true;
        "Show Statusbar Highlighting Mode" = true;
        "Show Statusbar Input Mode" = true;
        "Show Statusbar Line Column" = true;
        "Show Statusbar Tab Settings" = true;
        "Show Word Count" = true;
        "Smart Copy Cut" = true;
        "Statusbar Line Column Compact Mode" = true;
        "Text Drag And Drop" = true;
        "Vi Input Mode Steal Keys" = false;
        "Vi Relative Line Numbers" = false;
        "Word Completion" = true;
        "Word Completion Minimal Word Length" = 3;
        "Word Completion Remove Tail" = true;
      };
      KateSQLPlugin.SaveConnections = true;
      Konsole = {
        KonsoleEscKeyExceptions = "vi,vim,nvim,git;";
        AutoSyncronize = true;
        AutoSyncronizeMode = 1;
        KonsoleEscKeyBehaviour = true;
        RemoveExtension = false;
        SetEditor = true;
      };
      lspclient = {
        AutoHover = true;
        AutoImport = true;
        CompletionDocumentation = true;
        CompletionParens = true;
        Diagnostics = true;
        FormatOnSave = false;
        HighlightGoto = true;
        IncrementalSync = true;
        InlayHints = true;
        Messages = true;
        ReferencesDeclaration = true;
        SemanticHighlighting = true;
        SignatureHelp = true;
        SymbolDetails = true;
        SymbolExpand = true;
        SymbolSort = true;
        SymbolTree = true;
        TypeFormatting = true;
      };
      project = {
        autorepository = "git";
        gitStatusDoubleClick = 3;
        gitStatusSingleClick = 2;
        index = true;
        multiProjectCompletion = true;
        multiProjectGoto = true;
        restoreProjectsForSessions = true;
      };
    };
  };
}
