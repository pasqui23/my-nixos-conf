inputs:
{
  nixosConfig,
  pkgs,
  lib,
  config,
  ...
}:
{

  imports = [
    ./kate.nix
    ./qownnotes.nix
  ];
  manual = {
    html.enable = true;
    json.enable = true;
    manpages.enable = true;
  };
  programs = {
    mangohud.enable = true;
    obs-studio = {
      enable = true;
      plugins = with pkgs.obs-studio-plugins; [
        obs-backgroundremoval
        wlrobs
      ];
    };
    kodi = {
      enable = true;
      package = pkgs.my-kodi;
      datadir = "${config.xdg.dataHome}/kodi";
    };
  };
  services = {
    hound = {
      enable = true;
      databasePath = "${config.xdg.cacheHome}/hound";
    };
  };
  qt.kde.settings = {
    konsolerc = {
      UiSettings.ColorScheme = "Predefinito";
      TabBar = {
        NewTabButton = true;
        TabBarPosition = "Bottom";
        "Desktop Entry".DefaultProfile = "Profile 1.profile";
      };
    };
    arkrc.Extraction = {
      closeAfterExtraction = true;
      openDestinationFolderAfterExtraction = true;

    };
    breezerc = {
      Common = {
        OutlineCloseButton = true;
        ShadowSize = "ShadowNone";

      };
      Style = {
        DockWidgetDrawFrame = true;
        MenuOpacity = 51;
        SidePanelDrawFrame = true;
        TabBarDrawCenteredTabs = true;

      };
      Windeco = {
        DrawBackgroundGradient = true;
        DrawBorderOnMaximizedWindows = true;
        DrawSizeGrip = true;
        DrawTitleBarSeparator = true;
      };
    };
    kwinrc = {
      Desktops = {
        Number = 9;
        Rows = 3;
      };
      Compositing = {
        GLCore = true;
        LatencyPolicy = "ExtremelyLow";
        MaxFPS = 60;
        RefreshRate = 60;
        Enabled = true;
      };
      TabBox.LayoutName = "compact";
      Effect-PresentWindows.BorderActivateAll = 0;
      Windows = {
        ElectricBorderMaximize = false;
        ElectricBorders = 1;
        FocusPolicy = "FocusFollowsMouse";
        FocusStealingPreventionLevel = 0;
        NextFocusPrefersMouse = true;
      };
      "org.kde.kdecoration2" = {
        BorderSize = "None";
        BorderSizeAuto = false;
        ButtonsOnLeft = "MSLBN";
        ButtonsOnRight = "HFIAX";
        library = "org.kde.breeze";
        theme = "Brezza";
      };
      KDE = {
        Enabled = true;
        GLCore = true;
        LatencyPolicy = "ExtremelyLow";
        LayoutName = "compact";
        MaxFPS = 60;
      };
      Effect-DesktopGrid = {
        BorderActivate = 7;
        BorderWidth = 0;
      };
      Effect-blurplus = {
        BlurDecorations = true;
        BlurDocks = true;
        BlurMatching = false;
        BlurMenus = true;
        BlurNonMatching = true;
        WindowClasses = "";
      };
      Effect-wobblywindows = {
        Drag = 90;
        Stiffness = 6;
        WobblynessLevel = 2;
      };
      Effect-overview.BorderActivate = 9;
      Plugins = {
        blurEnabled = true;
        dimscreenEnabled = true;
        forceblurEnabled = true;
        magiclampEnabled = true;
        mousemarkEnabled = true;
        sheetEnabled = true;
        slidebackEnabled = true;
        snaphelperEnabled = true;
        squashEnabled = false;
        thumbnailasideEnabled = true;
        trackmouseEnabled = true;
        wobblywindowsEnabled = true;
      };
    };
    kcminputrc = {
      Mouse = {
        Effect-DesktopGrid = {
          BorderActivate = 7;
          BorderWidth = 0;
        };
        XLbInptAccelProfileFlat = true;
      };
    };
    plasmarc.Theme.name = "breeze";
    kdeglobals = {
      General = {
        BrowserApplication = "firefox.desktop";
        fixed = "Maple Mono,12,-1,5,50,0,0,0,0,0";
        font = "Atkinson Hyperlegible,14,-1,5,50,0,0,0,0,0";
        menuFont = "Atkinson Hyperlegible,14,-1,5,50,0,0,0,0,0";
        toolBarFont = "Atkinson Hyperlegible,14,-1,5,50,0,0,0,0,0";
        smallestReadableFont = "Atkinson Hyperlegible,11,-1,5,50,0,0,0,0,0";
        accentColorFromWallpaper = true;
      };
      WM.activeFont = "Atkinson Hyperlegible,10,-1,5,75,0,0,0,0,0,Bold";
      KDE = {
        LookAndFeelPackage = "Aritim-Dark";
        widgetStyle = "Breeze";
        AnimationDurationFactor = 0.25;
        ScrollbarLeftClickNavigatesByPage = false;
        SingleClick = true;
        Size = 16;

      };
      Toolbar = {
        ToolButtonStyle = "style";
        ToolButtonStyleOtherToolbars = "style";
      };
#       Icons.Theme = "Tela-black-dark";
      MainToolbarIcons.Size = 16;
      PanelIcons.Size = 16;
      ToolbarIcons.Size = 16;
      "Toolbar style" = {
        ToolButtonStyle = "NoText";
        ToolButtonStyleOtherToolbars = "NoText";
      };
    };
    dolphinrc = {
      VersionControl.enabledPlugins = "Bazaar,Dropbox,Git,Mercurial,Subversion";
      General = {
        AutoExpandFolders = true;
        BrowseThroughArchives = true;
        ShowCopyMoveMenu = true;
        ShowToolTips = true;
        UseTabForSwitchingSplitView = true;
      };
    };
    krunnerrc.Plugins = {
      CharacterRunnerEnabled = true;
      DictionaryEnabled = true;
      "Kill RunnerEnabled" = true;
      PowerDevilEnabled = true;
      "Spell CheckerEnabled" = true;
      appstreamEnabled = true;
      baloosearchEnabled = true;
      bookmarksEnabled = true;
      browserhistoryEnabled = true;
      browsertabsEnabled = true;
      calculatorEnabled = true;
      desktopsessionsEnabled = true;
      katesessionsEnabled = false;
      konsoleprofilesEnabled = true;
      kwinEnabled = true;
      locationsEnabled = true;
      marbleEnabled = true;
      "org.kde.activities2Enabled" = true;
      "org.kde.activitiesEnabled" = true;
      "org.kde.datetimeEnabled" = true;
      "org.kde.windowedwidgetsEnabled" = true;
      placesEnabled = true;
      plasma-desktopEnabled = true;
      plasma-runner-neochatEnabled = true;
      recentdocumentsEnabled = true;
      servicesEnabled = true;
      shellEnabled = true;
      skroogeaddoperationEnabled = true;
      unitconverterEnabled = true;
      webshortcutsEnabled = true;
      windowsEnabled = true;
      Favourites.plugins = "krunner_services";
    };
    rsibreakrc = {
      General.AutoStart = true;
      "General Settings" = {
        BigDuration = 15;
        BigInterval = 120;
        BigThreshold = 20;
        DisableAccel = false;
        Effect = 0;
        ExpandImageToFullScreen = true;
        Graylevel = 100;
        HideLockButton = false;
        HideMinimizeButton = false;
        HidePostponeButton = false;
        #         ImageFolder=/home/me;
        Patience = 60;
        PostponeBreakDuration = 2;
        SearchRecursiveCheck = false;
        ShowSmallImagesCheck = true;
        SlideInterval = 10;
        TinyDuration = 20;
        TinyInterval = 20;
        TinyThreshold = 60;
        UseNoIdleTimer = false;
        UsePlasmaReadOnly = false;
      };
      yakuakerc.Appearence = {
        BackgroundColorOpacity = 70;
        HideSkinBorders = true;
        TerminalHighlightOnManualActivation = true;
        Translucency = true;
      };

    };
    kglobalshortcuts = {
      yakuake.toggle-window-state = "F12,F12,Apri/chiudi Yakuake";
    };
    "plasma-org.kde.plasma.desktop-appletsrc".Containments = {
      "1" = {
        formfactor = 0;
        immutability = 1;
        lastScreen = 0;
        location = 0;
        plugin = "org.kde.plasma.folder";
        wallpaperplugin = "org.kde.potd";
        Wallpaper."org.kde.potd".General.Provider = "wcpotd";
      };
      "2" = {
        formfactor = 2;
        immutability = 1;
        lastScreen = 0;
        location = 4;
        plugin = "org.kde.panel";
        wallpaperplugin = "org.kde.image";
        Gemeral.AppletOrder = "3;4;26;7;20;29";
        Applets = {
          "20" = {
            plugin = "org.kde.plasma.digitalclock";
            Configuration = {
              PreloadWeight = 100;
              popupHeight = 720;
              popupWidth = 1440;
              Appearance = {
                enabledCalendarPlugins = "alternatecalendar,astronomicalevents,holidaysevents";
                showWeekNumbers = true;
              };
            };
          };
          "26".plugin = "org.kde.plasma.taskmanager";
          "29".plugin = "org.kde.plasma.colorpicker";
          "3" = {
            plugin = "org.kde.plasma.kickoff";
            Configuration = {
              PreloadWeight = 100;
              popupHeight = 1571;
              popupWidth = 2798;
              Shortcuts.global = "Alt+F1";
            };
          };
          "4".plugin = "org.kde.plasma.pager";
          "7" = {
            plugin = "org.kde.plasma.systemtray";
            Configuration = {

            };
          };
        };
      };
      "8" = {
        formfactor = 2;
        immutability = 1;
        lastScreen = 0;
        location = 4;
        plugin = "org.kde.plasma.private.systemtray";
        popupHeight = 1571;
        popupWidth = 2798;
        Applets = {
          "10".plugin = "org.kde.plasma.clipboard";
          "11".plugin = "org.kde.plasma.devicenotifier";
          "12".plugin = "org.kde.plasma.manage-inputmethod";
          "13".plugin = "org.kde.plasma.notifications";
          "14".plugin = "org.kde.plasma.keyboardindicator";
          "17".plugin = "org.kde.plasma.keyboardlayout";
          "18".plugin = "org.kde.plasma.printmanager";
          "19".plugin = "org.kde.plasma.volume";
          "22".plugin = "org.kde.plasma.brightness";
          "23".plugin = "org.kde.plasma.battery";
          "24".plugin = "org.kde.plasma.networkmanagement";
          "25".plugin = "org.kde.plasma.bluetooth";
          "26".plugin = "org.kde.plasma.mediacontroller";
          "39".plugin = "org.kde.plasma.katesessions";
          "9".plugin = "org.kde.plasma.cameraindicator";
          "15".plugin = "org.kde.kdeconnect";
          "16".plugin = "org.kde.kscreen";
        };
        General = {
          extraItems = "org.kde.plasma.battery,org.kde.plasma.brightness,org.kde.plasma.cameraindicator,org.kde.plasma.clipboard,org.kde.plasma.devicenotifier,org.kde.plasma.manage-inputmethod,org.kde.plasma.mediacontroller,org.kde.plasma.notifications,org.kde.plasma.keyboardindicator,org.kde.kdeconnect,org.kde.kscreen,org.kde.plasma.bluetooth,org.kde.plasma.keyboardlayout,org.kde.plasma.networkmanagement,org.kde.plasma.printmanager,org.kde.plasma.volume,org.kde.plasma.addons.katesessions";
          hiddenItems = "caffeine,RSIBreak,Yakuake,org.kde.plasma.brightness,org.kde.plasma.addons.katesessions";
          knownItems = "org.kde.plasma.battery,org.kde.plasma.brightness,org.kde.plasma.cameraindicator,org.kde.plasma.clipboard,org.kde.plasma.devicenotifier,org.kde.plasma.manage-inputmethod,org.kde.plasma.mediacontroller,org.kde.plasma.notifications,org.kde.plasma.keyboardindicator,org.kde.kdeconnect,org.kde.kscreen,org.kde.plasma.bluetooth,org.kde.plasma.keyboardlayout,org.kde.plasma.networkmanagement,org.kde.plasma.printmanager,org.kde.plasma.volume";
        };
      };
    };
  };
  gtk.iconTheme = {
    name = "Fluent-dark";
    package = pkgs.fluent-icon-theme;
  };
  fonts.fontconfig.enable = true;

  xdg.configFile."nix-init/config.toml".text = ''maintainers = ["pasqui23"]'';
  xdg.configFile."autostart/org.kde.yakuake.desktop".source =
    "${pkgs.yakuake}/share/applications/org.kde.yakuake.desktop";
  xdg.dataFile = {
    "konsole/ayu.colorscheme".source = ./ayu.colorscheme;
    "konsole/Base.profile".source = ./Base.profile;
    "color-schemes/BreezeBlack.colors".source = ./BreezeBlack.colors;
    "applications/mimeapps.list".source =
      config.lib.file.mkOutOfStoreSymlink "${config.xdg.configHome}/mimeapps.list";
  };
  home = {
    enableDebugInfo = true;
    pointerCursor = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Amber";
      size = 24;
      x11.enable = true;
      gtk.enable = true;
    };
    packages =
      with pkgs;
      [
        (writeScriptBin "fzf" ''
          exec skim "$@"
        '')
        adbfs-rootless
        android-file-transfer
        any-nix-shell
        ascii-draw
        apg
        atkinson-hyperlegible
        bespokesynth-with-vst2
        bruno
        bustle
        crosswords
        curl
        ddrescue
        ddrescueview
        devtoolbox
        dissent
        elf-dissector
        endless-sky
        ext4magic
        faba-mono-icons
        ffmpeg-full
        file
        foremost
        fretboard
        gallery-dl
        #         gaw
        gcs
        gh
        git-crypt
        gltron
        heimer
        heroic
        hibernate
        hieroglyphic
        iaito
        identity
        inputs.kwin-effects-forceblur.packages.${pkgs.system}.default
        julia
#         kazumi
        kdiff3
        kdiskmark
        keypunch
        (kodi-cli.override { youtube-dl = pkgs.yt-dlp; })
        letterpress
        liberation-circuit
        libinput
        libreoffice-qt
        lifeograph
        livecaptions
        lsof
        lynx
        lyrebird
        #         mandelbulber
        manix
        memento
        metronome
        minder
        mlt
        mousai
        mtr-gui
        mythes
        nix-init
        nixfmt-rfc-style
        noto-fonts-cjk-sans
        ntfs3g
        orca
        outfox
        paleta
        paperwork
        pciutils
        pdf4qt
        pencil
        pmutils
        #         pong3d
        portfolio
        pokete
        projectm
        protonup-qt
        pv
        qmmp
        qpwgraph
        rclone
        scrcpy
        seabird
        share-preview
        skypeforlinux
        sn0int
        sony-headphones-client
        spacenavd
        spectrojack
        speechd
        sshfs
        #         stepmania
        subsurface
        surfraw
#         tela-icon-theme
        tellico
        testdisk-qt
        thunderbird
        tor-browser-bundle-bin
        transmission_4-qt6
        trash-cli
        treesheets
        trayscale
        usbutils
        vistafonts-chs
        waylyrics
        waycheck
        webcamoid
        xpano
        zeal-qt6
        zrythm
      ]
      ++ (with hunspellDicts; [
        en-gb-large
        en-us-large
        en-ca-large
        en-au-large
        it-it
      ])
      ++ (with aspellDicts; [
        en-science
        en-computers
        en
        it
      ])
      ++ (with kdePackages; [
        accessibility-inspector
        ark
        blinken
        bomber
        bovo
        blinken
        calibre
        codevis
        digikam
        dolphin-plugins
        filelight
        gcompris
        granatier
        gwenview
        heaptrack
        hotspot
        itinerary
        kaffeine
        kalgebra
        kanagram
        kapman
        kasts
        katomic
        kblackbox
        kblocks
        kbounce
        kbreakout
        kcharselect
        kclock
        kdegraphics-thumbnailers
        kdenetwork-filesharing
        kdenlive
        kdeplasma-addons
        kdiamond
        kgeography
        kgeotag
        kget
        kgoldrunner
        kgraphviewer
        khangman
        kigo
        killbots
        kio
        kio-admin
        kio-extras
        kio-extras-kf5
        kio-fuse
        kio-zeroconf
        kiriki
        kiten
        kitinerary
        kjumpingcube
        kleopatra
        klickety
        kmbox
        kmime
        kmines
        kmouth
        knavalbattle
        knetwalk
        knights
        kolf
        kollision
        konquest
        konsole
        kpat
        krdp
        krecorder
        krename
        kreversi
        krohnkite
        kronometer
        kruler
        krunner-ssh
        krunner-symbols
        kshisen
        ksirk
        ksnakeduel
        kspaceduel
        ksquares
        kstars
        ksudoku
        ksystemlog
        ktrip
        kwalletmanager
        kweather
        labplot
        libksysguard
        lskat
        manuskript
        milou
        minuet
        okular
        PageEdit
        palapeli
        parley
        partition-manager
        peruse
        picmi
        plasma-hud
        plasma-nm
        plasma-systemmonitor
        plasma-workspace-wallpapers
        polkit-kde-agent
        print-manager
        qc
        qgo
        qjoypad
        qpwgraph
        qtkeychain
        rsibreak
        skanpage
        skrooge
        spectacle
        step
        sweeper
        systemdgenie
        systemsettings
        tellico
        tokodon
        virt-manager-qt
        yakuake

      ])
      ++ (with sweethome3d; [
        application
        furniture-editor
        textures-editor
      ]);
    activation.colorScheme = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      ${pkgs.kdePackages.plasma-workspace}/bin/plasma-apply-colorscheme BreezeBlack
    '';
  };
}
