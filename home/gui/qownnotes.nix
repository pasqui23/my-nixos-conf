{
  pkgs,
  config,
  lib,
  ...
}:
let
  scripts = [
    "ai-autocompletion"
    "ai-text-tool"
    "definition-lists"
    "dictionary-autocomplete"
    "in-note-text-tagging"
    "indent-line"
    "ollama-integration"
    "random-note"
    "render-plantuml"
    "scribble"
    "wiki-links"
    "zotero"
  ];
  source =
    pkgs.runCommand "qownnotes-scripts"
      {
        inherit scripts;
        outputs = [
          "out"
          "sql"
        ];
      }
      ''
        			mkdir -p $out
        			touch $sql
        			for s in $scripts;do
                d="${pkgs.inputs.qownnotes-scripts}/$s"
        				cp $d/$s.qml $out
        				echo "insert into script (name, script_path,enabled,priority,\"IDENTIFIER\",info_json,settings_variables_json )
                  select '$s','$out/$s.qml',1,1,'$s','$(sed 's/'/\\'/g s/\\/\\\\/'<$s/info.json)',null
                  where not exists (select 1 from script where name = '$s');" >>$sql
        			done
        		'';
in
{
  home.packages = with pkgs; [
    qownnotes
  ];
  xdg.dataFile."PBE/QOwnNotes/scripts" = {
    inherit source;
    recursive = true;
  };
  home.activation.qownnotes-script =
    lib.hm.dag.entryAfter [ "writeBoundary" ]
      ''${pkgs.sqlite}/bin/sqlite3 ${config.xdg.dataHome}/PBE/QOwnNotes/QOwnNotes.sqlite  "$(cat ${source.sql})"'';
  qt.kde.settings."PBE/QOwnNotes.conf" = {
    General = {
      ActiveNoteHistoryItem = null;
      LastUpdateCheck = null;
      NoteHistory-1 = null;
      SearchEngineId = 2;
      ShowSystemTray = false;
      StartHidden = false;
      # 				acceptAllExternalModifications=false;
      allowNoteEditing = true;
      # 				autoReadOnlyMode=false;
      autoReadOnlyModeTimeout = 30;
      automaticNoteFolderDatabaseClosing = false;
      checkSpelling = true;
      closeTodoListAfterSave = false;
      # 				cursorWidth=1;
      darkMode = false;
      # 				darkModeColors=true;
      darkModeIconTheme = false;
      darkModeTrayIcon = false;
      defaultNoteFileExtension = "md";
      demoNotesCreated = true;
      disableAutomaticUpdateDialog = true;
      disableSavedSearchesAutoCompletion = false;
      dockWasInitializedOnce = true;
      enableNoteTree = true;
      enableSocketServer = true;
      enableWebAppSupport = false;
      externalEditorPath = "${pkgs.kate}/bin/kate";
      fullyHighlightedBlockquotes = true;
      gitCommitInterval = 30;
      gitExecutablePath = "${pkgs.git}/bin/git";
      # gitLogCommand=;
      guiFirstRunInit = false;
      ignoreAllExternalModifications = false;
      ignoreAllExternalNoteFolderChanges = false;
      ignoreNoteSubFolders = ''^\\.'';
      imageScaleDown = false;
      imageScaleDownMaximumHeight = 1024;
      imageScaleDownMaximumWidth = 1024;
      initialLayoutIdentifier = "full";
      interfaceFontSize = 11;
      # interfaceLanguage=;
      interfaceStyle = "Breeze";
      internalIconTheme = false;
      itemHeight = 16;
      legacyLinking = false;
      markdownHighlightingEnabled = true;
      maxNoteFileSize = 1048576;
      navigationPanelAutoSelect = true;
      navigationPanelHideSearch = false;
      newNoteAskHeadline = false;
      noteEditIsCentralWidget = true;
      noteListPreview = false;
      noteSaveIntervalTime = 10;
      noteSubfoldersPanelDisplayAsFullTree = true;
      noteSubfoldersPanelHideSearch = false;
      noteSubfoldersPanelOrder = 0;
      noteSubfoldersPanelShowFullPath = false;
      noteSubfoldersPanelShowNotesRecursively = false;
      noteSubfoldersPanelShowRootFolderName = true;
      noteSubfoldersPanelSort = 0;
      noteSubfoldersPanelTabsUnsetAllNotesSelection = false;
      notesPanelOrder = 0;
      notesPanelSort = 1;
      notesPath = "${config.home.homeDirectory}/Note";
      notifyAllExternalModifications = false;
      overrideInterfaceFontSize = false;
      restoreCursorPosition = true;
      restoreLastNoteAtStartup = true;
      restoreNoteTabs = true;
      showMatches = true;
      showMenuBar = true;
      showStatusBar = true;
      showStatusBarNotePath = true;
      showStatusBarRelativeNotePath = false;
      spellCheckLanguage = "auto";
      startInReadOnlyMode = false;
      systemIconTheme = true;
      taggingShowNotesRecursively = true;
      tagsPanelHideNoteCount = false;
      tagsPanelHideSearch = false;
      todoCalendarSupport = true;
      useNoteFolderButtons = false;
      useUNIXNewline = false;
    };
    Editor = {
      CurrentSchemaKey = "EditorColorSchema-3fee9f7b-d91d-45cb-8a41-3cbc168f269e";
      autoBracketClosing = true;
      autoBracketRemoval = true;
      disableCursorBlinking = false;
      editorWidthInDFMOnly = true;
      highlightCurrentLine = true;
      indentSize = 2;
      removeTrailingSpaces = true;
      showLineNumbers = true;
      useTabIndent = true;
      vimMode = false;
    };
    ai = {
      currentBackend = "ollama";
      enabled = true;
    };
    localTrash = {
      autoCleanupDays = 30;
      autoCleanupEnabled = true;
      supportEnabled = true;

    };
  };
}
