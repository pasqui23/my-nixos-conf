inputs:
{
  config,
  lib,
  pkgs,
  os,
  ...
}:
{
  imports = [
    ./ssh.nix
  ];
  systemd.user.startServices = "sd-switch";
  home = {
    stateVersion = "22.11";
    sessionVariables = {
      HYPHEN_INSENSITIVE = "1";
      CASE_INSENSITIVE = "1";
      MOZ_X11_EGL = "1";
      MOZ_ENABLE_WAYLAND = "1";
      __GL_ExperimentalPerfStrategy = "1";
      FX_THEME = "4";
      SSH_ASKPASS_REQUIRE = "prefer";
      JAVA_HOME = pkgs.openjdk23;
      STEAM_EXTRA_COMPAT_TOOLS_PATH = "${config.home.homeDirectory}/.steam/root/compatibilitytools.d";
      COMPLETE_DOTS = "1";
      XONSH_COLOR_STYLE = "native";
      GTK_DEBUG = "portals";
      GTK_USE_PORTAL = 1;
    };
    shellAliases = {
      nix-i = "nix repl '<nixpkgs/nixos>' --impure";
      mv = "mv -i";
      cp = "cp -v --reflink=auto";
      clean = "trash -rfv *~ .*~ \\#*\\# .\\#*\\#";
      j = "jobs";
      sysc = "systemctl";
      anonfox = "firefox --new-instance --profile $(mktemp -d)";
      git-i = "${pkgs.gitAndTools.git-extras}/bin/git-ignore";
      unzip = "atool --extract --explain";
      viDl = "yt-dlp --all-subs --prefer-free-formats";
      muDl = "yt-dlp --embed-thumbnail --format=bestaudio";
      "..." = "cd ../..";
    };
    packages =
      with pkgs;
      [
        (writeScriptBin "rm-special-char" ''
          #!${perl}/bin/perl
          while (<>) {
            s/ \e[ #%()*+\-.\/]. |
              \r | # Remove extra carriage returns also
              (?:\e\[|\x9b) [ -?]* [@-~] | # CSI ... Cmd
              (?:\e\]|\x9d) .*? (?:\e\\|[\a\x9c]) | # OSC ... (ST|BEL)
              (?:\e[P^_]|[\x90\x9e\x9f]) .*? (?:\e\\|\x9c) | # (DCS|PM|APC) ... ST
              \e.|[\x80-\x9f] //xg;
              1 while s/[^\b][\b]//g;  # remove all non-backspace followed by backspace
            print;
          }
        '')
        ani-cli
        apg
        atool
        bottom
        buildah
        curl
        curlie
        dig.dnsutils
        dolt
        du-dust
        fq
        ftop
        git-crypt
        grex
        hss
        iftop
        jc
        jnv
        lf
        lm_sensors
        moreutils
        nerdfetch
        nix-output-monitor
        ntfs3g
        octosql
        onefetch
        peco
        pipet
        piper-tts
        powertop
        procs
        psmisc
        pspg
        pv
        restish
        rink
        ripgrep
        sd
        sic-image-cli
        sq
        sshfs
        surfraw
        termimage
        tlrc
        trash-cli
        tre-command
        up
        usbtop
        usql
        xcp
      ]
      ++ (with bat-extras; [
        batman
        batgrep
        #       batdiff
        batwatch
        prettybat
      ]);
  };
  programs = {
    yt-dlp = {
      enable = true;
      extraConfig = ''
        --continue
      '';
    };
    aria2.enable = true;
    #thefuck.enable = true;
    bat = {
      enable = true;
      config = {
        theme = "OneHalfDark";
      };
    };
    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };
    bash.enable = true;
    xonsh = {
      enable = true;
      extraPackages =
        ps: with ps; [
          woob
          extras
          nixpkgs
          #howdoi
        ];
    };
    carapace.enable = true;
    navi = {
      enable = true;
      settings = {
        finder.command = "skim";
        shell.command = "xonsh";
      };
    };
    zoxide = {
      enable = true;
      options = [
        "--hook"
        "prompt"
      ];
    };
    eza = {
      enable = true;
      enableXonshIntegration = true;
      extraOptions = [ "-1abghHimM@X" ];
      git = true;
      icons = true;
    };
    starship = {
      enable = true;
      settings = {
        format = "# $username$hostname$all$fill$sudo$jobs$time$shlvl$os$status$cmd_duration$shell$line_break$character";
        character = {
          success_symbol = "[;](bold green)";
          error_symbol = "[;](bold red)";
          vicmd_symbol = "[:](bold yellow)";
        };
        hostname.trim_at = "";
        fill.symbol = " ";
        os.disabled = false;
        directory = {
          truncation_length = 10;
          read_only_style = "";
          truncation_symbol = "…/";
          repo_root_style = "green";
          fish_style_pwd_dir_length = 4;
        };
        git_status = {
          modified = "M";
          staged = "S";
        };
        kubernetes.disabled = false;
        shlvl.disabled = false;
        shell.disabled = false;
        status = {
          disabled = false;
          success_symbol = "";
          pipestatus = true;
        };
        battery.disabled = true;
        time.disabled = false;
        sudo.disabled = false;
      };
    };

    skim = rec {
      enable = true;
      defaultOptions = [
        "--preview 'bat {}'"
        "--prompt ⟫"
      ];
      fileWidgetOptions = defaultOptions;
      changeDirWidgetOptions = [
        "--preview 'eza -1lTXbmM --smart-group --classify=always --tree --color=always --color-scale=all --icons=always {}'"
      ];
    };
    keychain.enable = true;
    gpg.enable = true;
    yazi.enable = true;
    atuin = {
      enable = true;
      settings = {
        update_check = false;
        search_mode = "skim";
        workspaces = true;
        style = "compact";
        show_preview = true;
        enter_accept = false;
        sync_address = "http://nassie:5678";
      };
    };
    nix-index.enable = true;
    git = {
      enable = true;
      lfs.enable = true;
      delta.enable = true;
      userName = lib.mkDefault "me";
      userEmail = lib.mkDefault "not@example.com";
      inherit (inputs.self.lib) ignores;
      extraConfig = {
        help.autocorrect = "immediate";
        fetch.parallel = 0;
        submodule.fetchJobs = 0;
        merge.guitool = "code";
        diff.guitool = "code";
        mergetool.code.cmd = "codium --wait --merge $REMOTE $LOCAL $BASE $MERGED";
        difftool.code.cmd = "codium --wait --diff $LOCAL $REMOTE";
        difftool.delta.cmd = "delta $LOCAL $REMOTE";
        diff.tool = "delta";
        pull.rebase = true;
        rebase.autoStash = true;
        core.compression = 9;
        core.createObject = "link";
        column.ui = "auto row dense";
        feature.experimental = true;
        feature.manyFiles = true;
        merge.autoStash = true;
        pack.threads = 0;
        submodule.recurse = true;
        diff.renames = "copy";
        init.defaultBranch = "main";
      };
    };
  };
}
