{
  config,
  pkgs,
  lib,
  ...
}:
{
  imports = [
    # ./tesi.nix
  ];
  networking = {
    networkmanager = {
      enable = true;
      settings.connection."connection.mdns" = true;
    };
    nameservers = [
      "194.242.2.2#dns.mullvad.net"
      "2620:fe::fe#dns11.quad9.net"
      "2620:fe::9#dns11.quad9.net"
      "9.9.9.9#dns11.quad9.net"
      "149.112.112.112#dns11.quad9.net"
      "84.200.69.80#resolver1.dns.watch"
      "2001:1608:10:25::1c04:b12f#resolver1.dns.watch"
      "84.200.70.40#resolver2.dns.watch"
      "2001:1608:10:25::9249:d69b#resolver2.dns.watch"
      "[2001:67c:28a4::]#anycast.censurfridns.dk"
      "91.239.100.100#anycast.censurfridns.dk"
      "[2001:a18:1::29]#kaitain.restena.lu"
      "158.64.1.29#kaitain.restena.lu"
      "[2a04:b900:0:100::37]:853#getdnsapi.net"
      "185.49.141.37:853#getdnsapi.net"
      "[2a02:c205:3001:4558::1]#fdns1.dismail.de"
      "80.241.218.68#fdns1.dismail.de"
      "[2a01:4f8:c17:739a::2]#fdns2.dismail.de"
      "159.69.114.157#fdns2.dismail.de"
      "[2a01:4f8:c0c:83ed::1]#dot1.applied-privacy.net"
      "146.255.56.98#dot1.applied-privacy.net"
      "[2001:610:1:40ba:145:100:185:15]#dnsovertls.sinodun.com"
      "145.100.185.15#dnsovertls.sinodun.com"
      "[2001:610:1:40ba:145:100:185:16]#dnsovertls1.sinodun.com"
      "145.100.185.16#dnsovertls1.sinodun.com"
      "[2606:4700:4700::1111]#cloudflare-dns.com"
      "1.1.1.1#cloudflare-dns.com"
    ];
    hosts."0.0.0.0" = [
      "log-upload-os.mihoyo.com"
      "overseauspider.yuanshen.com"
      "prd-lender.cdp.internal.unity3d.com"
      "thind-prd-knob.data.ie.unity3d.com"
      "thind-gke-usc.prd.data.corp.unity3d.com"
      "cdp.cloud.unity3d.com"
      "remote-config-proxy-prd.uca.cloud.unity3d.com"
    ];
    useHostResolvConf = false;
    wireless.enable = lib.mkForce false;
    hostFiles = with pkgs; [
      (stevenblack-hosts.override {
        flags = {
          extensions = [ "fakenews" ];
          compress = true;
        };
      })
    ];
  };
  services.resolved = {
    enable = true;
    dnssec = "false";
  };
}
