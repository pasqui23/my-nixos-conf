inputs:
{ pkgs, name, ... }@os:
{
  deployment.targetHost = name;
  hardware.enableRedistributableFirmware = true;
  hardware.bluetooth.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  time.timeZone = "Europe/Amsterdam";
  users.users.me = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [
      "wheel"
      "networkmanager"
      "podman"
      "boinc"
      "realtime"
    ];
  };
  environment = {
    enableDebugInfo = true;
    interactiveShellInit = ''
      cd(){
        mkdir -p "$1"
        builtin cd "$1"
      }
      if xset q &>/dev/null; then
        export EDITOR="kate -b"
        export PAGER="cat"
        exec $(qc completion bash)
      fi
      export VISUAL=$EDITOR
    '';
  };
  home-manager = {
    useUserService = true;
    useUserPackages = true;
    useGlobalPkgs = true;
    backupFileExtension = "hm~";
    verbose = true;
    sharedModules = [ inputs.self.homeModules.core ];
    extraSpecialArgs = { inherit os; };
    users = {
      me.home.sessionVariables.DEFAULT_USER = "me";
      root.home.sessionVariables.DEFAULT_USER = "root";
    };
  };
  nix = {
    #     package = pkgs.lixVersions.lix_2_91;
    gc = {
      automatic = true;
      options = "--delete-older-than 7d";
      dates = "Sun *-*-1..7 03:00";
      randomizedDelaySec = "14m";
    };
    settings = {
      trusted-users = [ "@wheel" ];
      auto-optimise-store = true;
      narinfo-cache-negative-ttl = 0;
      connect-timeout = 5;
      log-lines = 25;
      min-free = 128000000;
      max-free = 1000000000;
      fallback = true;
      warn-dirty = false;
    };
    nixPath = [ "nixos-config=${./configuration.nix}" ];
  };
  services.printing.enable = true;
  hardware.sane.enable = true;
  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "22.10"; # Did you read the comment?
}
