inputs:
{
  config,
  lib,
  pkgs,
  name,
  ...
}:
{
  options.my.downtime = lib.mkOption {
    type = lib.types.str;
    default = "21:30";
  };
  config = {
    networking.hostName = name;
    environment.systemPackages = [
      (pkgs.runCommand "mount.rclone" { } ''
        mkdir -p $out/bin
        ln -s ${pkgs.rclone}/bin/rclone $out/bin/mount.rclone
      '')
    ];
    # fileSystems."/mnt/pcloud" = {
    #   device = "resource:/";
    #   fsType = "rclone";
    #   options = [
    #     "noauto"
    #     "x-systemd.automount"
    #     "x-systemd.mount-timeout=30"
    #     "_netdev"
    #     "x-systemd.idle-timeout=1h"
    #     "vfs-cache-mode=writes"
    #     "config=${config.deployment.keys.rclone.path or ""}"
    #     "allow-other"
    #     "default-permissions"
    #     "max-read-ahead=16M"
    #     "uid=1000"
    #   ];
    # };
    deployment.keys.borg-id = {
      keyFile = ./key/borg-id;
      #       destDir = "${config.users.users.me.home}/.config";
      user = "me";
      group = "users";
    };
    deployment.keys.borg-pass = {
      keyFile = ./key/borg-pass;
      #       destDir = "${config.users.users.me.home}/.config";
      user = "me";
      group = "users";
    };
    deployment.keys.rclone = {
      keyFile = ./key/rclone.conf;
      destDir = "/var/keys";
    };
    services.borgbackup.jobs.${name} = {
      environment.BORG_RSH = "${pkgs.openssh}/bin/ssh -i ${config.deployment.keys.borg-id.path}";
      doInit = true;
      repo = "borg@nassie:/mnt/raid1/borg/${name}";
      startAt = config.my.downtime;
      compression = "zstd";
      paths = [ config.users.users.me.home ];
      encryption = {
        mode = "repokey-blake2";
        passCommand = "cat ${config.deployment.keys.borg-pass.path}";
      };
      exclude =
        inputs.self.lib.ignores
        ++ (lib.attrsets.mapAttrsToList (
          n: v: config.users.users.me.home + "/" + n
        ) config.home-manager.users.me.home.file);
      prune = {
        keep = {
          within = "1d"; # Keep all archives from the last day
          daily = 7;
          weekly = 4;
          monthly = -1; # Keep at least one archive for each month
        };
        prefix = config.services.borgbackup.jobs.${name}.archiveBaseName;
      };
      user = "me";
      persistentTimer = true;
    };
  };
}
