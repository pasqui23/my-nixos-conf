inputs:
{ lib, ... }:
{
  inherit (inputs.self.lib) nixpkgs;
  nix =
    let
      i = builtins.removeAttrs inputs [ "self" ];
    in
    {
      nixPath = lib.attrsets.mapAttrsToList (n: v: "${n}=${toString v}") i;
      registry = lib.attrsets.mapAttrs (n: v: {
        from = {
          id = n;
          type = "indirect";
        };
        to = {
          type = "path";
          path = toString v;
        };
      }) i;

    };
}
