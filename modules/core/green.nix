{ config, pkgs, ... }:
{
  services = {
    sysstat.enable = true;
    smartd.enable = true;
    switcherooControl.enable = true;
    system76-scheduler = {
      enable = true;
      settings.cfsProfiles.enable = true;
    };
    power-profiles-daemon.enable = true;
    acpid = {
      enable = true;
      handlers.ac-power =
        let
          sc = x: "${pkgs.systemd}/bin/systemctl ${x} acPower.target";
        in
        {
          action = ''
            vals=($1)  # space separated string to array of multiple values
            case ''${vals[3]} in
            00000000)
              ${sc "stop"}
            ;;
            00000001)
              ${sc "start"}
            ;;
            esac
          '';
          event = "ac_adapter/*";
        };
    };
  };
  systemd.targets.acPower.description = "Target indicating connected to AC";
  systemd.services = builtins.listToAttrs (
    map
      (name: {
        inherit name;
        value = {
          wantedBy = [ "acPower.target" ];
          partOf = [ "acPower.target" ];
        };
      })
      [
        "boinc"
        "avahi-daemon"
        "beesd@"
        "languagetool"
        "peerix"
        "snowflake-proxy"
        "ollama"
      ]
  );
  environment.systemPackages = with config.boot.kernelPackages; [ turbostat ];
  networking.networkmanager = {
    wifi = {
      powersave = true;
      macAddress = "random";
      backend = "iwd";
    };
    ethernet = {
      macAddress = "random";
    };
  };
  boot.kernel.sysctl = {
    "vm.laptop_mode" = "5";
    "vm.swappiness" = 1;
    "vm.vfs_cache_pressure" = 30;
    "vm.dirty_ratio" = 30;
    "vm.dirty_backgound_ratio" = 10;
    "vm.dirty_writeback_centisecs" = 6000;
    "kernel.nmi_watchdog" = 0;
  };
  boot.kernelParams = [
    "i915.i915_enable_fbc=1"
    "btusb.enable_autosuspend=y"
    "workqueue.power_efficient=y"
  ];

  boot.extraModprobeConfig = ''
    options v4l2loopback exclusive_caps=1 video_nr=1
    options snd_hda_intel power_save=1
    options usbcore autosuspend=1
    # Intel VPRO remote access technology driver
    blacklist mei_me
  '';

  fileSystems."/boot".options = [
    "noauto"
    "x-systemd.automount"
  ];
}
