{
  config,
  pkgs,
  lib,
  ...
}:
{
  zramSwap = {
    enable = true;
    algorithm = "zstd";
    memoryPercent = 100;
  };
  boot.kernelModules = [
    "fuse"
    "rtl8723be"
  ];
  boot.extraModprobeConfig = ''
    options rtl8723be ant_sel=2
  '';
  #   services.btrfs.autoScrub.enable = true;
  boot.tmp.cleanOnBoot = true;
  #   services.udev.extraRules = ''
  #     ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"
  #     SUBSYSTEMS=="usb", ACTION=="add", RUN+="${pkgs.systemd}/bin/systemd-mount -AG --no-block --discover --timeout-idle-sec=10 /dev/%k '/media/%E{NAME}'"
  #     SUBSYSTEMS=="usb", ACTION=="remove", RUN+="${pkgs.systemd}/bin/systemd-mount --umount --no-block /dev/%k"
  #   '';
}
