{ pkgs, config, ... }:
{
  # For mount.cifs, required unless domain name resolution is not needed.
  environment.systemPackages = [ pkgs.cifs-utils ];
  fileSystems."/mnt/file-share/public" = {
    device = "//file-share/public";
    fsType = "cifs";
    options = # this line prevents hanging on network split
      [
        "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,credentials=${config.deployment.keys.smb.path}"
      ];
  };
  deployment.keys.smb.keyFile = ./key/smb;
}
