{
  self,
  dwarffs,
  nixpkgs,
  srvos,
  ...
}:
{
  imports = [
    ./key
    (self.lib.modWithInputs ./backup.nix)
    (self.lib.modWithInputs ./base.nix)
    ./green.nix
    ./mnt.nix
    ./net.nix
    ./ssh.nix
    # (self.lib.modWithInputs ./peerix.nix)
    ./boinc.nix
    ./mail.nix
    (self.lib.modWithInputs ./flakes.nix)
    (self.lib.modWithInputs ./home.nix)
    #(self.lib.modWithInputs ./nix-ld.nix)
    ./programs.nix
    ./smb.nix
    dwarffs.nixosModules.dwarffs
    nixpkgs.nixosModules.notDetected
    srvos.nixosModules.mixins-systemd-boot
    srvos.nixosModules.mixins-terminfo
    srvos.nixosModules.mixins-nix-experimental
    srvos.nixosModules.mixins-trusted-nix-caches
    srvos.nixosModules.mixins-mdns
  ];
}
