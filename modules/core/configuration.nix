{
  inherit
    ((import (builtins.fetchGit "https://github.com/pasqui23/flake-compat" { src = ../.; }))
      .result.legacyPackages.${builtins.currentSystem}.nixosConfigurations.${builtins.readFile /etc/hostname}
    )
    config
    options
    imports
    ;
}
