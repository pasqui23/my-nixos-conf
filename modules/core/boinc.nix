{ pkgs, ... }:
{

  services.boinc = {
    enable = true;
    extraEnvPackages = (with pkgs; [ ocl-icd ]);
  };
  deployment.keys."account_www.worldcommunitygrid.org.xml" = {
    keyFile = ./key/account_www.worldcommunitygrid.org.xml;
    path = "/var/lib/boinc/account_www.worldcommunitygrid.org.xml";
    destDir = "/var/keys";
  };
  services.snowflake-proxy.enable = true;
}
