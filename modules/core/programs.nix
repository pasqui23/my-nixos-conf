{ pkgs, ... }:
{
  virtualisation = {
    libvirtd = {
      # enable = true;
      qemu.runAsRoot = false;
    };
    virtualbox.host = {
      #       enable = true;
      enableExtensionPack = true;
    };
    waydroid.enable = true;
    podman = {
      enable = true;
      dockerCompat = true;
      dockerSocket.enable = true;
    };
  };
  systemd.tmpfiles.rules = [ "L! /var/run/docker.sock - - - - /run/podman/podman.sock" ];
  systemd.extraConfig = "DefaultTimeoutStopSec=10s";
  security = {
    allowUserNamespaces = true;
    apparmor = {
      enable = true;
      packages = [ pkgs.apparmor-profiles ];
      enableCache = true;
    };
  };
  users.users.me.shell = "/etc/profiles/per-user/me/bin/xonsh";
  programs = {
    extra-container.enable = true;
    zsh.enable = true;
    adb.enable = true;
    iotop.enable = true;
    criu.enable = true;
    zmap.enable = true;
    traceroute.enable = true;
    mtr.enable = true;
    java.enable = true;
    wireshark.enable = true;
    kdeconnect.enable = true;
    npm.enable = true;
    gnupg.dirmngr.enable = true;
    gnupg.agent = {
      enableBrowserSocket = true;
      enable = true;
    };
    dconf.enable = true;
    bandwhich.enable = true;
    usbtop.enable = true;
    iftop.enable = true;
    liboping.enable = true;
  };
  services.dbus = {
    apparmor = "enabled";
    implementation = "broker";
  };
  systemd.services.boinc.serviceConfig = {
    CPUAccounting = true;
    CPUQuota = "75%";
  };
  boot.tmp.useTmpfs = true;
  systemd.services.nix-daemon = {
    environment.TMPDIR = "/var/tmp";
    serviceConfig = rec {
      CPUWeight = 20;
      IOWeight = CPUWeight;
    };
  };
}
