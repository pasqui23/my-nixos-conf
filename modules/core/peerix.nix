inputs:
{ config, ... }:
{
  imports = [
    inputs.peerix.nixosModules.peerix
  ];
  services.peerix = {
    enable = true;
    privateKeyFile = config.deployment.keys.nix-cache-priv.path;
    publicKeyFile = ./key/nix/pub;
  };
  deployment.keys.nix-cache-priv = {
    destDir = "/var/keys";
    keyFile = ./key/nix/priv;
  };
}
