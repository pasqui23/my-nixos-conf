{
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "yes";
    #     settings.PasswordAuthentication = false;
    startWhenNeeded = true;
  };
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    publish = {
      enable = true;
      addresses = true;
    };
  };
  services.tailscale.enable = true;
  deployment.allowLocalDeployment = true;
}
