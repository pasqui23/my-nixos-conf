{ pkgs, ... }:
{
  systemd = {
    packages = [
      (pkgs.writeTextDir "lib/systemd/system/service.d/mail.conf" ''
        [Unit]
        OnFailure=unit-status-mail@%n.service
      '')
    ];
    services."unit-status-mail@" = {
      environment = {
        HOSTNAME = "%H";
        MACHINE_ID = "%m";
        BOOT_ID = "%b";
        MAILTO = "root";
        MAILFROM = "unit-status-mailer";
        UNIT = "%I";
      };
      script = ''

        case $UNIT in unit-status-mail@*)
          #avoid infinite loop
          exit 0
        esac
        EXTRA=("Hostname: $HOSTNAME" "Machine ID: $MACHINE_ID" "Boot ID: $BOOT_ID")
        UNITSTATUS=$(systemctl status $UNIT)

        sendmail $MAILTO <<EOF || true
        From:$MAILFROM
        To:$MAILTO
        Subject:Status mail for unit: $UNIT

        Status report for unit: $UNIT

        $EXTRA

        $UNITSTATUS
        EOF

        echo -e "Status mail sent to: $MAILTO for unit: $UNIT"
      '';
      after = [ "network.target" ];
      serviceConfig = {
        Type = "simple";
      };
    };
  };
}
