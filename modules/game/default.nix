_:
{ pkgs, ... }:
{
  programs.steam = {
    enable = true;
    gamescopeSession.enable = true;
    extest.enable = true;
    protontricks.enable = true;
  };
  environment.systemPackages = with pkgs; [
    celeste64
    katawa-shoujo-re-engineered
    freeciv_qt
    adwsteamgtk
    steam-tui
    itch

    space-cadet-pinball
    lutris
    ultrastardx
    ultrastar-creator
    ultrastar-manager
    vectoroids

    wineWowPackages.staging
  ];
}
