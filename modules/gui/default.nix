inputs:
{
  config,
  pkgs,
  lib,
  ...
}:
{
  console = {
    font = "Lat2-Terminus16";
    keyMap = "it";
  };
  i18n = {
    defaultLocale = "it_IT.UTF-8";
    inputMethod = {
      enable = true;
      type = "fcitx5";
      fcitx5.addons = with pkgs; [
        fcitx5-table-other
        fcitx5-mozc
      ];
    };
  };
  documentation = {
    enable = true;
    dev.enable = true;
    nixos.enable = true;
    nixos.includeAllModules = false;
    nixos.options.warningsAreErrors = false;
  };
  fonts = {
    enableDefaultPackages = true;
    enableGhostscriptFonts = true;
    fontconfig = {
      hinting.autohint = true;
    };
  };
  services = {
    xserver.enable = true;
    gnome.at-spi2-core.enable = true;
    irqbalance.enable = true;
    languagetool = {
      enable = true;
      allowOrigin = "*";
    };
    gpm.enable = true;
    flatpak.enable = true;
    displayManager = {
      autoLogin.user = "me";
      defaultSession = "plasma";
      sddm = {
        enable = true;
        settings = {
          General = {
            DisplayServer = "wayland";
            Numlock = "on";
          };
          Wayland.CompositorCommand = "${pkgs.westonLite}/bin/weston --shell=fullscreen-shell.so";
        };
      };
    };
    desktopManager.plasma6.enable = true;
    pipewire = {
      jack.enable = true;
      alsa.support32Bit = true;
      extraConfig.pipewire = {
        name = "libpipewire-module-rtkit";
        args = {
          nice.level = -11;
          rt.prio = 88;
          rt.time.soft = 2000000;
          rt.time.hard = 2000000;
        };
        flags = [
          "ifexists"
          "nofail"
        ];
      };
    };
  };
  boot = {
    kernelParams = [ "preempt=full" ];
    extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];
  };
  hardware.bluetooth.enable = true;

  programs.firefox = {
    enable = true;
    nativeMessagingHosts.packages = [ pkgs.firefoxpwa ];
  };
  environment.systemPackages = [ pkgs.firefoxpwa ];
  home-manager.sharedModules = [ inputs.self.homeModules.gui ];
  xdg.portal = {
    enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-kde ];
    config.common.default = "*";
    xdgOpenUsePortal = true;
  };
}
