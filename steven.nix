{
  stdenvNoCC,
  fetchFromGitHub,
  python3,
  lib,
  src,
  flags ? { },
  allowList ? "",
}:

stdenvNoCC.mkDerivation {
  name = "StevenBlack-hosts";

  inherit allowList src;
  passAsFile = [ "allowList" ];
  buildPhase = ''
    mv $allowListPath whitelist
    ${python3.withPackages (ps: with ps; [ requests ])}/bin/python \
      updateHostsFile.py \
      ${lib.cli.toGNUCommandLineShell { } (
        flags
        // {
          auto = true;
          noupdate = true;
          out = ".";
        }
      )}
  '';

  installPhase = ''
    mv ./hosts $out
  '';

  meta = with lib; {
    homepage = "https://github.com/StevenBlack/hosts";
    description = "Extending and consolidating hosts files from well-curated sources";
    # While the python script may be MIT
    # the various sources are under various licenses
    # not all host sources are under free licenses,let alone permissive one
    # https://github.com/StevenBlack/hosts#sources-of-hosts-data-unified-in-this-variant
    license = licenses.unfreeRedistributable;
    platforms = platforms.all;
    maintainers = [ mantainers.pasqui23 ];
  };
}
