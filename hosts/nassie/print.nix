inputs:
{ config, pkgs, ... }:
let
  name = "M2070-Series";
in
{
  #   imports = [
  #     "${inputs.nixpkgs}/pkgs/misc/cups/drivers/samsung/1.00.36/module.nix"
  #   ];
  services.printing.drivers = [ pkgs.samsung-unified-linux-driver_1_00_36 ];
  hardware.sane.extraBackends = [ pkgs.samsung-unified-linux-driver_1_00_36 ];
  hardware.printers = {
    ensureDefaultPrinter = name;
    ensurePrinters = [
      {
        inherit name;
        deviceUri = "usb://Samsung/M2070%20Series?serial=ZF44B8KJ5B0175T&interface=1";
        model = "Samsung_M2070_Series.ppd.gz";
        ppdOptions = {
          PageSize = "A4";
        };
      }
    ];
  };

  services.avahi.publish.enable = true;
  services.avahi.publish.userServices = true;
  services.printing.browsing = true;
  services.printing.listenAddresses = [ "*:631" ]; # Not 100% sure this is needed and you might want to restrict to the local network
  services.printing.allowFrom = [ "all" ]; # this gives access to anyone on the interface you might want to limit it see the official documentation
  services.printing.defaultShared = true; # If you want

  networking.firewall.allowedUDPPorts = [ 631 ];
  networking.firewall.allowedTCPPorts = [ 631 ];
}
