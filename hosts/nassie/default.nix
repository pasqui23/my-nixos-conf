{ self, srvos, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ./update.nix
    ./borg.nix
    ./raid.nix
    ./atuin.nix
    (self.lib.modWithInputs ./print.nix)
    #     srvos.nixosModules.server
  ];
}
