{ pkgs, config, ... }:
let
  spec = "/mnt/raid1";
in
{
  boot.swraid.mdadmConf = ''
    ARRAY /dev/md0 metadata=1.2 name=giottequis:0 UUID=ea79a6ac:6654bc5b:07a91931:ec648659
  '';
  fileSystems.${spec} = {
    device = "/dev/disk/by-uuid/073a72e3-e02d-4f3e-92e4-95ce9e39c3c5";
    fsType = "btrfs";
    options = [
      "noauto"
      "x-systemd.automount"
      "x-systemd.mount-timeout=30"
      "x-systemd.idle-timeout=3min"
      "compress=zstd"
    ];
  };
  #   services.beesd.filesystems.raid1 = { inherit spec; };
  systemd.services.pcloud-backup = {
    startAt = config.my.downtime;
    serviceConfig.ExecStart = "${pkgs.rclone}/bin/rclone --config ${config.deployment.keys.rclone.path} sync /mnt/raid1/borg resource:/borg/";
  };
  systemd.services.remountRaid = {
    before = [ "mnt-raid1.mount" ];
    wants = [ "mnt-raid1.automount" ];
    path = with pkgs; [
      utillinux
      mdadm
    ];
    #TODO: there must be a better way …
    script = ''
      if ls /mnt/raid1 &> /dev/null; then
        exit 0
      fi
      umount /mnt/raid1 /dev/md*
      mdadm --stop /dev/md*
      mdadm --assemble --scan -v
      systemctl restart mnt-raid1.{,auto}mount
    '';
  };
  systemd.packages = [
    (pkgs.writeTextDir "lib/systemd/system/mnt-raid1.mount.d/remount.conf" ''
      [Unit]
      OnFailure=remountRaid.service
    '')
  ];
  systemd.services.noBackLight = {
    wantedBy = [ "multi-user.target" ];
    serviceConfig.ExecStart = "${pkgs.brillo}/bin/brillo -S 0";
  };
  virtualisation.oci-containers.containers.lancachenet = {
    image = "lancachenet/monolithic";
    ports = [
      "80:80"
      "433:433"
    ];
  };
}
