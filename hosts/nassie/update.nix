{
  pkgs,
  config,
  nodes,
  lib,
  ...
}:
{
  services.logind = rec {
    lidSwitch = "ignore";
    lidSwitchExternalPower = lidSwitch;
    lidSwitchDocked = lidSwitch;
  };
  home-manager.users.root.programs.git.extraConfig.core.sshCommand =
    "ssh -o IdentitiesOnly=yes -i ${config.deployment.keys.git.path} -F /dev/null";
  programs.ssh.extraConfig = ''
    Host pasqui23.gitlab.com
      Hostname gitlab.com
      PreferredAuthentications publickey
      IdentityFile ${config.deployment.keys.git.path}
  '';
  deployment.keys = {
    git = {
      destDir = "/var/keys";
      keyFile = ./key/git;
    };
    prv-gpg = {
      destDir = "/var/keys";
      keyFile = ./key/prv.gpg;
    };
    pub-gpg = {
      destDir = "/var/keys";
      keyFile = ./key/pub.gpg;
    };
  };
  systemd.packages = [
    (pkgs.runCommand "colmena-override-dir" { } ''
      o="$out/lib/systemd/system"
      mkdir -p $o
      touch $o/colmena-{,upgrade-}.service
    '')
  ];
  systemd.services =
    let
      colmena = "${pkgs.nix}/bin/nix run .";

      cfgUnit = {
        serviceConfig = rec {
          CPUWeight = 20;
          IOWeight = 20;
          WorkingDirectory = "/etc/nixos";
        };
        environment = {
          HOME = config.users.users.root.home;
          NIX_PATH = lib.strings.concatStringsSep ":" config.nix.nixPath;
        };
        wants = [ "network-online.target" ];
        after = [ "network-online.target" ];
      };
    in
    {
      gpg-key-import = rec {
        path = [ pkgs.gnupg ];
        wantedBy = [
          "prv-gpg-key.service"
          "pub-gpg-key.service"
        ];
        after = wantedBy;
        script = ''
          gpg --import ${config.deployment.keys.pub-gpg.path}
          gpg --allow-secret-key-import --import ${config.deployment.keys.prv-gpg.path}
        '';
      };
      colmena-upgrade = _: {
        imports = [ cfgUnit ];
        startAt = config.my.downtime;
        after = [
          "gpg-key-import.service"
          "nix-gc.service"
        ];
        path = with pkgs; [
          config.nix.package
          git
          git-crypt
          openssh
        ];
        environment.NIXPKGS_ALLOW_UNFREE = "1";
        script = ''
          git pull
          nix flake update
          nix flake check
          ${colmena} build
          git commit -am "update : $(date)"
          git push
        '';
        onFailure = [ "colmena-reset.service" ];
      };
      colmena-reset = _: {
        imports = [ cfgUnit ];
        serviceConfig.ExecStart = "${pkgs.git}/bin/git restore .";
      };
    }
    // lib.mapAttrs' (
      n: v:
      lib.nameValuePair "colmena-upgrade-${n}" (_: {
        imports = [ cfgUnit ];
        after = [ "colmena-upgrade.service" ];
        environment.NIXPKGS_ALLOW_UNFREE = "1";
        startAt = v.config.my.downtime;
        serviceConfig.ExecStart = "${colmena} apply --on ${n}";
      })
    ) nodes;
}
