{
  config,
  pkgs,
  lib,
  nodes,
  ...
}:
let
  path = "/mnt/raid1/borg";
in
{
  deployment.keys.borgPub = {
    keyFile = ./key/borg-id.pub;
    destDir = "/var/keys";
  };
  systemd.services.backup-sync = {
    startAt = config.my.downtime;
    after = [ "borgbackup-job-me.service" ];
    serviceConfig = {
      Type = "oneshot";
      ExecStart = [
        "${pkgs.rclone}/bin/rclone --config ${config.deployment.keys.rclone.path} copyto ${path} resource:/backup"
      ];
    };
    unitConfig.RequiresMountsFor = path;
  };
  my.downtime = "03:00";
  services.borgbackup.repos = lib.mkMerge (
    lib.mapAttrsToList (n: v: {
      "${n}" = {
        path = "${path}/${n}";
        authorizedKeys = [
          config.deployment.keys.borgPub.path
        ];
      };
    }) nodes
  );
}
