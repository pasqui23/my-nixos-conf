inputs:
{ pkgs, ... }:
{
  imports = [
    ./hardware-configuration.nix
    inputs.self.nixosModules.gui
  ];
  environment.systemPackages = with pkgs; [
    kdePackages.kate
    thunderbird
    qownnotes
    krita
    krita-plugin-gmic
    borgbackup
    xournalpp
    glaxnimate
  ];
  services.xserver.enable = true;
  my.downtime = "13:00";
}
