inputs:
{
  config,
  pkgs,
  lib,
  ...
}:
{
  my.downtime = "21:30";
  boot.loader.systemd-boot.extraEntries."win.conf" = ''
    title Windows
    efi   /EFI/BOOT/BOOTX64.EFI
  '';
  home-manager.users.me.programs.git = {
    signing = {
      key = "0x13160FFB4CEB03F2";
      signByDefault = true;
    };
    userName = "paki23";
    userEmail = "pk93@posteo.net";
  };
  home-manager.users.guest = { };
  users.users = {
    me.extraGroups = [ "lp" ];
    guest = {
      isNormalUser = true;
      password = "";
    };
  };
  hardware.graphics.extraPackages = with pkgs; [
    libva
  ];
  #   services.beesd.filesystems.root = {
  #     spec = "/";
  #     extraOptions = [ "-g" "0.2" ];
  #   };
  services.fstrim.enable = true;
  systemd.services.game-backup = {
    wantedBy = [ "borgbackup-job-me.service" ];
    before = [ "borgbackup-job-me.service" ];
    serviceConfig = {
      User = "me";
      ExecStart = [ "${pkgs.ludusavi}/bin/ludusavi backup" ];
    };
  };
}
