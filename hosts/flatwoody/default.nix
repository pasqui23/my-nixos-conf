{
  self,
  nixos-hardware,
  srvos,
  ...
}:
{
  imports = [
    ./hardware-configuration.nix
    ./nvidia.nix
    (self.lib.modWithInputs ./ai.nix)
    (self.lib.modWithInputs ../nassie/print.nix)
    (self.lib.modWithInputs ./programs.nix)
    nixos-hardware.nixosModules.asus-rog-strix-g733qs
    #     srvos.nixosModules.desktop
    self.nixosModules.gui
    self.nixosModules.game
  ];
}
