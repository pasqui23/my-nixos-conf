{ nixified-ai, ... }:
{ config, pkgs, ... }:
let
  OLLAMA_HOST = "127.0.0.1:${toString config.services.ollama.port}";
  OLLAMA_API_BASE = "http://localhost:${toString config.services.ollama.port}";
in
{
  imports = [ nixified-ai.nixosModules.comfyui ];
  services.ollama = {
    enable = true;
    acceleration = "cuda";
    port = 8008;
  };
  environment.sessionVariables = { inherit OLLAMA_HOST; };
  services.postgresql = {
    enable = true;
    ensureUsers = [
      {
        name = "plandex";
        ensureDBOwnership = true;
        ensureClauses.login = true;
      }
    ];
    ensureDatabases = [ "plandex" ];
  };
  users.users.plandex.isSystemUser = true;
  users.users.plandex.group = "plandex";
  users.groups.plandex = { };
  systemd.services.plandex = {
    wantedBy = [ "multi-user.target" ];
    after = [ "postgresql.service" ];
    path = with pkgs; [ git ];
    environment = {
      GOENV = "development";
      TZ = "GMT"; # TODO: set plandex's and pg's TZ=UTC
      DATABASE_URL = "postgres://plandex@/plandex?host=/run/postgresql/"; # TODO: how does postgres decides the path?
      inherit OLLAMA_API_BASE;
    };
    serviceConfig = {
      ExecStart = "${pkgs.plandex-server}/bin/plandex-server";
      User = "plandex";
    };
  };
  services.comfyui.enable = true;
  services.open-webui = {
    enable = true;
    port = 8888;
    environment = {
      inherit OLLAMA_API_BASE;
      RAG_EMBEDDING_ENGINE = "ollama";
      COMFYUI_BASE_URL = "http://localhost:8188";
      SEARXNG_QUERY_URL = "https://www.gruble.de/?preferences=eJx1WE2P4zgO_TWbi9HB9s4Aiz3ktMBeZ4Cdu0FLjM22JLolOYnr1w_lj5guVx8qFVESRT2Sj1QMZGw5EqZbiwEjuIuD0I7Q4g2cDNiAwxuGC4yZDfvBYcZbE-GBF_KyrB4iv6bbX3HEi8fcsb39-cf__7okuGNCiKa7_fOSO_R4S1S2XyKm0eVUc6gDPusMze1_4BJeLFMtk-weGG8MMrxybC_LtjrlSSwpZlwMhoyxBkdt8PJ9OR3sA4JBW6_HLkp_jhinmkKdKcv-eSWFOwXKotNEdm4RLruKVWYBZRJFDs2qvePc45RuFu8g5l8sJWicnIahpSD4_aeFtq4TGwJXebQE__jXf8ENMlk5CuOrGsD0gliqa8plLgRIVTmUHljXd3KYinjoK08xctQyQaCSzypljnpxAMPBwqYy9XW9OlKGDTkqf3X9IIucZlFuRtNjXnc0Q3PYkS217a5_vsfVGHO1qLSYYYh4x4iC96pIkExJhAKBoSJfZE_7IHGyOmLC4dOwmgOpwDL_X4USGyIqn0XTGB3hwVLoQsd3LbKIHxIWtR8TmXmcMmQSLSwBGIvEtpU4sDifOCS9WUyejOM1CvSEXIKs0iHwlb-WqwUPhYuaO5iv5E-EomjXt_jWwyDL5bOc6PkHDQX5fVWWMJoOAHx_KcDkSjZ3kL3kmRZHxCrxPT8hYmUpSjiXwF5cdo8UegKjN0yTumqL9NFJFimJJBE0634ZoAQH-23MbCOC1euZW7nd4GAq4Zv2yNIzniVINI5eMjCWPCIzu-ltXzuwtdrHHQgXlY_Vhk5yDOMC_iLA1ztCRWu6Eq9j8u0YlW4KoEygIF-Jx_S1bDv_B6WO9_tKvkWIU1XclEhdt0xgjhCSEyu0Hx03KeM1bgYLiYIZIGxD_uhIr_dP3zgtCMPmgDAB7EeynRLqrOUBhS4zKnuLKOLAytKFpShtDCGZfk6Kt_Ccu_vUlh5fbTukxzA2wi-P9cCI1lI-UWnknIXrWWKDUYXYotBPiY64CkGbMaXrMElR2tA0YO1UQtaPwgFbIgz0rWOHJ_mi2rDFqny8pR5CJlMlI7sgasbL3E-cWWKiLx7cMM0FG1kOo7ZQS1cksrfNfrUnNZPeIKX4BcHGQgy79MnuHsFLqekO8u-__fbv125Dwo8A_hBJ_AOxP0vOPl3lB6fNSTuQ47wbHKQtiFphHJupRb_F9oAY89jooJzdK9v7Uiee2KipCczh-mV8ti2Okg9ao5NymiSKtezJL-o5SJRVaQocpkKTewRhH4-XW0SnsxbxifbTj-E6PIPSKJlusrY9PdqSZ0rXg8nOfcGKzZN68lKZDjcevXfTgV52okg8RoPdmHf6PeRa315b3vLqguHYrsxZuPQl5eStgYgveuiAbiTeDPhhZ7yyei1OutMI7efxGb5ZesB5lpzwLKwkgZR0KTMd3nsuLLSBIT1BaUCmYwvwIElOiFlttVxqQtWNjYLaQj7cYC_Rh-KfJ89BYFLhcreRS0OwJdbdkel1IZHtpGtEW2hrqzpSNHdDCqs0GNvdg607kNhaJU9IrvIDlqvshOYq_4KtuvW8tRxq6unlTpB0AUPvp2pjyLlcfyLoZcWYMP5qTupM_tVc0Sw4nac_vq1F9VhMS5Xlu2wLklhJYyPeah8H33qQttly-IVx7-kOUiek_MUK-y7H9JIucbS7d6UY9IRb_1FCF6TPKuGVcL7OG-5tTsqdhAysLehxXpgDMUsfuDWDgy10uS8a5CFSytk6S-UlhEmH-1DSU_l_Hl9nc3erByrdUAMKVWmtjCAhnWRpmbZAEGN9UacsmIaNLn4-Jdk00rPgGJaL6BTBi_gUrBGkwaoa6bmTbvOSVBrY2pCdd3kUdjq6Q1p-07Owx93xc38Wjc0Y8riFegEtSJ-1vL2-yIw0DhjH9HZrKpQylOeu5nR2ZKWfjkvcbjiOIUlXmzr9noHuSM6z4IjTxOOn0viWvB80QE74qASOWvYgX1q6vdYJux00i6BsmVNoby0kwyXlJASvJ9_oyZOH9CSMtrTTujRsUzMaOyiWTP7gcMDPSxX28hyp3n2xPTKyRVVTnxxtoF49iZhzefcUJ33K1yc410kvEvSlco5XUgrnHzEORasIzmgs4hMOi_hYzfLv29t5_31gcKNU3HQr0fu6rqNrJ1Qo7djaVegZhnr5IeQZpaM9TUu81VILTf-u6F8fYMCZUTDleFJRaG7ddZpL6O41hTufD47z06qWl7gY50uKndaU0lALJcucZNhFumFhtNvfvsbxww==&q=<query>";
      ENABLE_RAG_WEB_SEARCH = "True";
      RAG_WEB_SEARCH_ENGINE = "searxng";
      RAG_WEB_SEARCH_RESULT_COUNT = "3";
      RAG_WEB_SEARCH_CONCURRENT_REQUESTS = "10";
      WEBUI_URL = "http://localhost:80";

    };
  };
  programs.alvr = {
    enable = true;
    openFirewall = true;
  };
  environment.systemPackages = with pkgs; [
    plandex
    open-interpreter
    code2prompt
    llm
# #     promptfoo
    crow-translate
    semgrep
    src-cli
    fabric-ai
    #     screen-pipe
  ];
}
