{ pkgs, config, ... }:

{
  services.xserver.videoDrivers = [ "nvidia" ];

  services.switcherooControl.enable = true;
  services.boinc.extraEnvPackages = [ config.boot.kernelPackages.nvidia_x11 ];

  hardware.nvidia = {
    modesetting.enable = true;
    powerManagement = {
      finegrained = true;
      enable = true;
    };
    prime.offload = {
      enable = true;
      enableOffloadCmd = true;
    };
  };
  virtualisation.kvmgt = {
    enable = true;
    device = "0000:0a:00.0";
  };
  hardware.asus.battery.chargeUpto = 60;
  boot.extraModulePackages = with config.boot.kernelPackages; [ asus-wmi-sensors ];
  services.pipewire.configPackages = [
    (pkgs.runCommand "pipewire-config" { } ''
      install -Dt $out/pipewire/pipewire.conf.d ${./10-internal.conf}
      install -Dt $out/pipewire/client.conf.d ${./10-client.conf}
    '')
  ];
  services.asusd = {
    enable = true;
    enableUserService = true;
  };
  programs.rog-control-center.enable = true;
  nixpkgs.config.nvidia.acceptLicense = true;
  environment.systemPackages = with pkgs.nvtopPackages; [
    #     nvidia
    amd
  ];
}
