{
  description = "Shared config for my PCs.";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    srvos = {
      url = "github:nix-community/srvos";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    peerix = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:cid-chan/peerix";
    };
    home-manager = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:pasqui23/home-manager/xonsh";
    };
    nix-ld = {
      url = "github:Mic92/nix-ld";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    dwarffs.inputs.nixpkgs.follows = "nixpkgs";
    stevenblack-hosts = {
      url = "github:StevenBlack/hosts";
      flake = false;
    };
    kwin-effects-forceblur = {
      url = "github:taj-ny/kwin-effects-forceblur";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixified-ai = {
      url = "github:nixified-ai/flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    qownnotes-scripts = {
      url = "github:qownnotes/scripts";
      flake = false;
    };
  };

  outputs =
    { self, ... }@inputs:
    {

      overlays.default = final: prev: {
        inherit inputs;
        stevenblack-hosts = final.callPackage ./steven.nix {
          src = inputs.stevenblack-hosts;
          allowList = "click.discord.com";
        };

        tesseract = prev.tesseract4;
        vaapiIntel = prev.vaapiIntel.override { enableHybridCodec = true; };
        my-kodi = prev.kodi-wayland.withPackages (
          p: with p; [
            netflix
            youtube
            sendtokodi
            vfs-libarchive
            steam-launcher
            steam-controller
            inputstream-adaptive
            inputstream-ffmpegdirect
            visualization-starburst
          ]
        );
        /*
                ofxstatement-bare = prev.callPackage ./ofxstatement.nix { };
                ofxstatement-n26 = prev.python3.pkgs.callPackage ./ofxstatement-n26.nix { };
                ofxstatement =
                  with final;
                  symlinkJoin {
                    name = "ofxstatement";
                    paths = [
                      ofxstatement-bare
                      ofxstatement-n26
                    ];
                  };
        */
        apps = {
          default = {
            type = "app";
            program = toString (
              final.writeShellScript "colmena" ''
                export NIXPKGS_ALLOW_UNFREE=1
                exec ${final.colmena}/bin/colmena "$@" --impure
              ''
            );
          };
          apply = {
            type = "app";
            program = toString (
              final.writeShellScript "colmena-apply" ''
                exec sudo ${final.apps.default.program} apply -v "$@"
              ''
            );
          };
          apply-local = {
            type = "app";
            program = toString (
              final.writeShellScript "colmena-apply-local" ''
                exec sudo ${final.apps.default.program} apply-local -v --node $(
                  cat /etc/hostname) "$@"
              ''
            );
          };
        };
      };

      lib = {
        ignores = import ./ignore.nix;
        nixpkgs = {
          overlays = [
            self.overlays.default
            inputs.peerix.overlay
          ];
          system = "x86_64-linux";
          config.allowUnfree = true;
          config.permittedInsecurePackages = [ "olm-3.2.16" ];
        };
        modWithInputs = _file: {
          inherit _file;
          imports = [ (import _file inputs) ];
        };
        importDir = d: builtins.mapAttrs (n: v: self.lib.modWithInputs /${d}/${n}) (builtins.readDir d);
      };
      legacyPackages.x86_64-linux = import inputs.nixpkgs self.lib.nixpkgs;
      apps.x86_64-linux = self.legacyPackages.x86_64-linux.apps;
      formatter.x86_64-linux = self.legacyPackages.x86_64-linux.treefmt2;

      homeModules = self.lib.importDir ./home;
      nixosModules = self.lib.importDir ./modules;
      colmena =
        {
          network = {
            nixpkgs = self.legacyPackages.x86_64-linux;
            specialArgs = { inherit inputs; };
          };
        }
        // builtins.mapAttrs (n: v: {
          imports = [
            v
            (self.lib.modWithInputs ./modules/core)
          ];
        }) (self.lib.importDir ./hosts);
    };
}
